#Con clases:
import random

class Board:
    def __init__(self):
        self.tablero = [[" " for _ in range(3)] for _ in range(3)]
        self.turno = "X"  # Comienza el jugador X

    def mostrar_tablero(self):
        for fila in self.tablero:
            print("|".join(fila))
            print("-" * 5)

    def es_movimiento_valido(self, fila, columna):
        if fila < 0 or fila >= 3 or columna < 0 or columna >= 3:
            return False
        if self.tablero[fila][columna] != " ":
            return False
        return True

    def realizar_movimiento(self, fila, columna):
        if self.es_movimiento_valido(fila, columna):
            self.tablero[fila][columna] = self.turno
            self.turno = "O" if self.turno == "X" else "X"
        else:
            print("Movimiento inválido. Inténtalo de nuevo.")

    def hay_ganador(self):
        for i in range(3):
            if self.tablero[i][0] == self.tablero[i][1] == self.tablero[i][2] != " ":
                return True
            if self.tablero[0][i] == self.tablero[1][i] == self.tablero[2][i] != " ":
                return True
        if self.tablero[0][0] == self.tablero[1][1] == self.tablero[2][2] != " ":
            return True
        if self.tablero[0][2] == self.tablero[1][1] == self.tablero[2][0] != " ":
            return True
        return False

    def esta_lleno(self):
        for fila in self.tablero:
            if " " in fila:
                return False
        return True

    def reiniciar(self):
        self.tablero = [[" " for _ in range(3)] for _ in range(3)]
        self.turno = "X"

    def obtener_movimiento_maquina(self):
        # Búsqueda en profundidad para tomar decisiones del movimiento de la máquina
        mejor_puntuacion = float("-inf")
        mejor_movimiento = None

        for i in range(3):
            for j in range(3):
                if self.tablero[i][j] == " ":
                    self.tablero[i][j] = "O"  # Simula el movimiento de la máquina
                    puntuacion = self.minimax(self.tablero, 0, False)
                    self.tablero[i][j] = " "  # Deshace el movimiento

                    if puntuacion > mejor_puntuacion:
                        mejor_puntuacion = puntuacion
                        mejor_movimiento = (i, j)

        return mejor_movimiento

    def minimax(self, tablero, profundidad, es_maximizador):
        if self.hay_ganador():
            return 1 if es_maximizador else -1
        elif self.esta_lleno():
            return 0

        if es_maximizador:
            mejor_puntuacion = float("-inf")
            for i in range(3):
                for j in range(3):
                    if tablero[i][j] == " ":
                        tablero[i][j] = "O"  # Simula el movimiento de la máquina
                        puntuacion = self.minimax(tablero, profundidad + 1, False)
                        tablero[i][j] = " "  # Deshace el movimiento
                        mejor_puntuacion = max(mejor_puntuacion, puntuacion)
            return mejor_puntuacion
        else:
            mejor_puntuacion = float("inf")
            for i in range(3):
                for j in range(3):
                    if tablero[i][j] == " ":
                        tablero[i][j] = "X"  # Simula el movimiento del jugador
                        puntuacion = self.minimax(tablero, profundidad + 1, True)
                        tablero[i][j] = " "  # Deshace el movimiento
                        mejor_puntuacion = min(mejor_puntuacion, puntuacion)
            return mejor_puntuacion


# Crear una instancia de la clase Board
tablero = Board()

# Loop principal del juego
while True:
    # Mostrar el tablero antes del movimiento del jugador humano
    tablero.mostrar_tablero()

    # Turno del jugador humano
    fila = int(input("Ingresa la fila (0-2): "))
    columna = int(input("Ingresa la columna (0-2): "))

    # Realizar el movimiento del jugador humano
    tablero.realizar_movimiento(fila, columna)

    # Mostrar el tablero después del movimiento del jugador humano
    tablero.mostrar_tablero()

    # Verificar si el jugador humano ha ganado o hay un empate
    if tablero.hay_ganador():
        print("¡Has ganado!")
        break
    elif tablero.esta_lleno():
        print("¡Empate!")
        break

    # Turno de la máquina
    print("Turno de la máquina...")

    # Obtener el movimiento de la máquina
    fila_maquina, columna_maquina = tablero.obtener_movimiento_maquina()

    # Realizar el movimiento de la máquina
    tablero.realizar_movimiento(fila_maquina, columna_maquina)

    # Verificar si la máquina ha ganado o hay un empate
    if tablero.hay_ganador():
        print("¡La máquina ha ganado!")
        break
    elif tablero.esta_lleno():
        print("¡Empate!")
        break