#Arbol tiktaktoe
class Nodo:
    def __init__(self, tablero, jugador, movimiento):
        self.tablero = tablero
        self.jugador = jugador
        self.movimiento = movimiento
        self.hijos = []

    def agregar_hijo(self, hijo):
        self.hijos.append(hijo)
# Crear el nodo raíz con el tablero vacío y el jugador X
tablero_inicial = [[' ' for _ in range(3)] for _ in range(3)]
nodo_raiz = Nodo(tablero_inicial, 'X', None)

# Generar los nodos hijos para todas las posibles jugadas siguientes
for fila in range(3):
    for columna in range(3):
        if tablero_inicial[fila][columna] == ' ':
            # Realizar el movimiento en el tablero actual
            tablero_hijo = [fila[:] for fila in tablero_inicial]
            tablero_hijo[fila][columna] = 'X' if nodo_raiz.jugador == 'O' else 'O'

            # Crear el nodo hijo correspondiente
            nodo_hijo = Nodo(tablero_hijo, 'O' if nodo_raiz.jugador == 'X' else 'X', (fila, columna))

            # Agregar el nodo hijo al nodo raíz
            nodo_raiz.agregar_hijo(nodo_hijo)

# Imprimir el árbol de nodos
def imprimir_arbol(nodo, nivel=0):
    print(' ' * nivel + str(nodo.movimiento) + ' - ' + nodo.jugador)
    for hijo in nodo.hijos:
        imprimir_arbol(hijo, nivel + 1)

imprimir_arbol(nodo_raiz)
