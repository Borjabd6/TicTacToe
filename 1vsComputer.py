import random

# Función para mostrar el tablero
def mostrar_tablero(tablero):
    for fila in tablero:
        print("|".join(fila))
        print("------")
    print("..........")    
# Función para verificar si alguien ha ganado
def verificar_ganador(tablero, jugador):
    # Verificar filas
    for fila in tablero:
        if all(casilla == jugador for casilla in fila):
            return True

    # Verificar columnas
    for columna in range(3):
        if all(tablero[i][columna] == jugador for i in range(3)):
            return True

    # Verificar diagonales
    if (tablero[0][0] == tablero[1][1] == tablero[2][2] == jugador) or (tablero[0][2] == tablero[1][1] == tablero[2][0] == jugador):
        return True

    return False

# Función para que la máquina realice su movimiento
def movimiento_maquina(tablero):
    # Comprobar si la máquina puede ganar en el siguiente movimiento
    for fila in range(3):
        for columna in range(3):
            if tablero[fila][columna] == " ":
                tablero[fila][columna] = "O"
                if verificar_ganador(tablero, "O"):
                    return

                tablero[fila][columna] = " "

    # Comprobar si el jugador puede ganar en el siguiente movimiento y bloquearlo
    for fila in range(3):
        for columna in range(3):
            if tablero[fila][columna] == " ":
                tablero[fila][columna] = "X"
                if verificar_ganador(tablero, "X"):
                    tablero[fila][columna] = "O"
                    return

                tablero[fila][columna] = " "

    # Si no hay movimientos críticos, hacer un movimiento aleatorio
    movimientos_disponibles = [(fila, columna) for fila in range(3) for columna in range(3) if tablero[fila][columna] == " "]
    fila, columna = random.choice(movimientos_disponibles)
    tablero[fila][columna] = "O"

# Función para jugar al Tic Tac Toe contra la máquina
def jugar_tic_tac_toe():
    # Inicializar el tablero
    tablero = [[" " for _ in range(3)] for _ in range(3)]
    jugador_actual = "X"
    juego_terminado = False

    # Bucle principal del juego
    while not juego_terminado:
        # Mostrar el tablero
        mostrar_tablero(tablero)

        if jugador_actual == "X":
            # Pedir al jugador que ingrese su movimiento
            fila = int(input("Ingrese el número de fila (0, 1 o 2): "))
            columna = int(input("Ingrese el número de columna (0, 1 o 2): "))

            # Verificar si la casilla está disponible
            if tablero[fila][columna] != " ":
                print("¡Esa casilla ya está ocupada! Inténtalo de nuevo.")
                continue

            # Realizar el movimiento del jugador
            tablero[fila][columna] = jugador_actual
        else:
            # Hacer el movimiento de la máquina
            movimiento_maquina(tablero)

        # Verificar si el jugador actual ha ganado
        if verificar_ganador(tablero, jugador_actual):
            mostrar_tablero(tablero)
            if jugador_actual == "X":
                print("¡Has ganado!")
            else:
                print("¡La máquina ha ganado!")
            juego_terminado = True
        # Verificar si hay empate
        elif all(casilla != " " for fila in tablero for casilla in fila):
            mostrar_tablero(tablero)
            print("¡Es un empate!")
            juego_terminado = True

        # Cambiar al siguiente jugador
        jugador_actual = "O" if jugador_actual == "X" else "X"

# Iniciar el juego
jugar_tic_tac_toe()
